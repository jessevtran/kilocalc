# KiloCalc

[ ![KiloCalc Logo](public/logo.png){width=150} ](https://kilocalc-3fx35.ondigitalocean.app/)

This repository added a few quality of life features to the original KiloCalc App:
- Weight Incrementer
- Button highlighting on activated options
- Decimal input for mobile devices

Also changed a few miscellaneous items:
- Changed yarn to npm
- Removed gitlab ci/cd because it was causing problems

If there are issues building and running the app locally, try adding the following to the package.json scripts:
```
--openssl-legacy-provider
```

Like so:
```
"scripts": {
    "start": "react-scripts --openssl-legacy-provider start",
    "build": "react-scripts --openssl-legacy-provider build",
},

```


---


![KiloCalc App Screenshot](public/app_screenshot.png){height=500}
